#include <avr/sleep.h>

const int interruptPin = 2;
const int interrupt = 1;

int count = 0;

void setup() {
  pinMode(interruptPin, INPUT);
  Serial.begin(115200);
  while(!Serial);
}

void loop() {
  count += 1;
  Serial.println(count);
  delay(1000);

  if(count == 10){
    sleepNow();
  }
}

void wake()
{
  sleep_disable ();  
  detachInterrupt(interrupt);
  count = 0;
}

void sleepNow()
{
  Serial.println("Going to sleep");
  Serial.println("");
  delay(1000);     
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);   
  sleep_enable();
  noInterrupts ();
  attachInterrupt (interrupt, wake, CHANGE);
  interrupts();
  sleep_cpu();
}