# Testing MTCH6102 for project Zeno

## Logic for loop
### 0. Set MODE to Gesture only
### 1. Read the bit when gesture present in loop with delay x.
### 2. If gesture present, read the gesture.
### 3. Print the gesture in Serial and send it over BLE.

## Logic for interrupt
### 0. Set MODE to Gesture only
### 1. Set interrupt pin.
### 2. Call function that reads the GESTURESTATE (id of the gesture detected)
### 3. Send the gesture id over BLE

## Notes
- switched to MOSI pin, as the RX did not work for interrupt-change, ref: https://forums.adafruit.com/viewtopic.php?f=24&t=91881

## Bugs
- the interrupt works (PIN 9 and INT5)
- the ISR is not called though :/