#include <Wire.h>

//VARIABLES from MTCH6102
// The default I2C address
#define MTCH6102_I2CADDR_DEFAULT   0x25

#define MTCH6102_MODE              0x05
#define MTCH6102_MODE_TOUCH   0x02
#define MTCH6102_MODE_FULL    0x03
#define MTCH6102_MODE_GESTURE 0x01
#define MTCH6102_MODE_RAW     0x04
#define MTCH6102_MODE_STANDBY 0x00

#define MTCH6102_NUMBEROFXCHANNELS 0x20
#define MTCH6102_NUMBEROFYCHANNELS 0x21
#define MTCH6102_TOUCHSTATE        0x10
#define MTCH6102_TOUCHX            0x11
#define MTCH6102_TOUCHY            0x12
#define MTCH6102_TOUCHLSB          0x13

#define MTCH6102_SENSORVALUE_RX0   0x80
#define MTCH6102_SENSORVALUE_RX1   0x81
#define MTCH6102_SENSORVALUE_RX2   0x82
#define MTCH6102_SENSORVALUE_RX3   0x83
#define MTCH6102_SENSORVALUE_RX4   0x84
#define MTCH6102_SENSORVALUE_RX5   0x85
#define MTCH6102_SENSORVALUE_RX6   0x86
#define MTCH6102_SENSORVALUE_RX7   0x87
#define MTCH6102_SENSORVALUE_RX8   0x88
#define MTCH6102_SENSORVALUE_RX9   0x89
#define MTCH6102_SENSORVALUE_RX10  0x8A
#define MTCH6102_SENSORVALUE_RX11  0x8B
#define MTCH6102_SENSORVALUE_RX12  0x8C
#define MTCH6102_SENSORVALUE_RX13  0x8D
#define MTCH6102_SENSORVALUE_RX14  0x8E

#define MTCH6102_GESTURESTATE      0x14

#define GES_BIT 1

//-------------------------------------

// PINS for Interrupt -----------------

const byte interruptPin = 11;
const int interrupt = 7;

//-------------------------------------

  byte data;
  byte gesture_present;

// read register
unsigned char readRegister(uint8_t addr_reg) {
    byte error;
    Wire.beginTransmission(MTCH6102_I2CADDR_DEFAULT);
    Wire.write(addr_reg); // register to read
    error = Wire.endTransmission();
    
    // Uncomment to DEBUG
    //Serial.print(millis());
    //Serial.print("  end transmit: ");
    //Serial.println(error,HEX); 

    Wire.requestFrom(MTCH6102_I2CADDR_DEFAULT, 1); // read a byte

    while (Wire.available()) {
        return Wire.read();
    }
}

// write data to register 
unsigned char writeRegister(uint8_t addr_reg, uint8_t dta) {
    Wire.beginTransmission(MTCH6102_I2CADDR_DEFAULT);
    Wire.write(addr_reg); // register to read
    Wire.write(dta);
    Wire.endTransmission();
}

volatile byte gesture_id;
volatile int counter = 0;

void setup() {
 byte data;

  Wire.begin();
  Serial.begin(115200);
  delay(3000);

  /*                              *
   * ******* MTCH6102 SETUP *******                          
   */

  // READ: MODE
  Serial.print("MODE: ");
  Serial.println(readRegister(MTCH6102_MODE), HEX);
  
  // Set mode to Gesture only
  Serial.println(MTCH6102_MODE_GESTURE, HEX);
  writeRegister(MTCH6102_MODE, MTCH6102_MODE_GESTURE);
  
  //CHECK: MODE
  Serial.print("MODE: ");
  Serial.println(readRegister(MTCH6102_MODE), HEX);

  // SET: NUMBER OF X CHANNELS
  writeRegister(MTCH6102_NUMBEROFXCHANNELS, 0x09);
  // SET: NUMBER OF Y CHANNELS
  writeRegister(MTCH6102_NUMBEROFYCHANNELS, 0x06);

   pinMode(interruptPin, INPUT_PULLUP);
   Serial.print("Interrupt at start: ");
   Serial.println(digitalRead(interruptPin));
   attachInterrupt(digitalPinToInterrupt(interruptPin), readGesture, FALLING);
   Serial.println("----------");
   Serial.println();
   Serial.println();
}

void loop() {
// TOUCHSTATE

  //uint8_t regMode = readRegister(MTCH6102_TOUCHSTATE);
  
  //DEBUG
  //Serial.print("TOUCHSTATE: ");
  //Serial.println(regMode, HEX);*/
  //-----
  
  //Read the bit for GES (bit 1)
  //gesture_present = bitRead(regMode, GES_BIT);
  //Serial.print("GES: ");
  //Serial.println(gesture_present, BIN);

  /*if (gesture_present == 1) {
    gesture_id = readRegister(MTCH6102_GESTURESTATE);
    Serial.println(gesture_id, BIN);
  }*/

    // DEBUG CODE for testing interrupt values
    Serial.print("Int0: ");
    Serial.println(digitalRead(interruptPin));
    Serial.println("Touch now. Waiting 5 seconds...");
    delay(5000);
    Serial.print("Int1: ");
    Serial.println(digitalRead(interruptPin));
    Serial.println("Reading register...");
    gesture_id = readRegister(MTCH6102_GESTURESTATE);
    Serial.print("GESTURE: ");
    Serial.println(gesture_id, HEX);
    readRegister(MTCH6102_GESTURESTATE);
    Serial.print("COUNTER: ");
    Serial.println(counter);
    Serial.println("----------");
    Serial.println();
    //----------
}

void addOne () {
  counter = counter+1;
}

void readGesture() {
  //Serial.println("kokot");
  gesture_id = readRegister(MTCH6102_GESTURESTATE);
  //Serial.println(gesture_id, BIN);
  counter = counter+1;
}
