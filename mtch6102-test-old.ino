#include <Wire.h>

//VARIABLES from MTCH6102
// The default I2C address
#define MTCH6102_I2CADDR_DEFAULT   0x25

#define MTCH6102_MODE              0x05
#define MTCH6102_MODE_TOUCH   0x02
#define MTCH6102_MODE_FULL    0x03
#define MTCH6102_MODE_GESTURE 0x01
#define MTCH6102_MODE_RAW     0x04
#define MTCH6102_MODE_STANDBY 0x00

#define MTCH6102_NUMBEROFXCHANNELS 0x20
#define MTCH6102_NUMBEROFYCHANNELS 0x21
#define MTCH6102_TOUCHSTATE        0x10
#define MTCH6102_TOUCHX            0x11
#define MTCH6102_TOUCHY            0x12
#define MTCH6102_TOUCHLSB          0x13

#define MTCH6102_SENSORVALUE_RX0   0x80
#define MTCH6102_SENSORVALUE_RX1   0x81
#define MTCH6102_SENSORVALUE_RX2   0x82
#define MTCH6102_SENSORVALUE_RX3   0x83
#define MTCH6102_SENSORVALUE_RX4   0x84
#define MTCH6102_SENSORVALUE_RX5   0x85
#define MTCH6102_SENSORVALUE_RX6   0x86
#define MTCH6102_SENSORVALUE_RX7   0x87
#define MTCH6102_SENSORVALUE_RX8   0x88
#define MTCH6102_SENSORVALUE_RX9   0x89
#define MTCH6102_SENSORVALUE_RX10  0x8A
#define MTCH6102_SENSORVALUE_RX11  0x8B
#define MTCH6102_SENSORVALUE_RX12  0x8C
#define MTCH6102_SENSORVALUE_RX13  0x8D
#define MTCH6102_SENSORVALUE_RX14  0x8E

#define MTCH6102_GESTURESTATE      0x14

//-------------------------------------

// read register
unsigned char readRegister(unsigned char addr_reg) {
    byte error;
    Wire.beginTransmission(MTCH6102_I2CADDR_DEFAULT);
    Wire.write(addr_reg); // register to read
    error = Wire.endTransmission();
   // Serial.print(millis());
//Serial.print("  end transmit: ");
    //Serial.println(error,HEX); 

    Wire.requestFrom(MTCH6102_I2CADDR_DEFAULT, 1); // read a byte

    while (Wire.available()) {
        return Wire.read();
    }
}

// write to register 
unsigned char writeRegister(uint8_t addr_reg, uint8_t dta) {
    Wire.beginTransmission(MTCH6102_I2CADDR_DEFAULT);
    Wire.write(addr_reg); // register to read
    Wire.write(dta);
    Wire.endTransmission();
}

void setup() {
 byte data;

  Wire.begin();
  Serial.begin(115200);

  delay(2000);

  /*                              *
   *                              *
   * ******* MTCH6102 SETUP *******
   *                              *                             
   *                              *
   */

  // READ: MODE
  data = readRegister(MTCH6102_MODE);
  Serial.print("MODE: ");
  Serial.println(data,BIN);
  
  // Set mode to Gesture only
  writeRegister(MTCH6102_MODE, MTCH6102_MODE_GESTURE);

  //CHECK: MODE
  data = readRegister(MTCH6102_MODE);
  Serial.print("MODE: ");
  Serial.println(data,BIN);

  // READ: NUMBER OF X CHANNELS
  data = readRegister(0x20);
  Serial.print("NUMBEROFXCHANNELS: ");
  Serial.println(data);
  
  // READ: NUMBER OF Y CHANNELS
  data = readRegister(0x21);
  Serial.print("NUMBEROFYCHANNELS: ");
  Serial.println(data);

  // SET: NUMBER OF X CHANNELS
  writeRegister(MTCH6102_NUMBEROFXCHANNELS, 0x09);
  // SET: NUMBER OF Y CHANNELS
  writeRegister(MTCH6102_NUMBEROFYCHANNELS, 0x06);

  //CHECK: NUMBER OF X CHANNELS
  data = readRegister(0x20);
  Serial.print("NUMBEROFXCHANNELS: ");
  Serial.println(data);

  //CHECK: NUMBER OF Y CHANNELS
  data = readRegister(0x21);
  Serial.print("NUMBEROFYCHANNELS: ");
  Serial.println(data);
}

void loop() {
  byte data;
  byte gesture_present;
  byte gesture_id;

  /*data = readRegister(0x15);
    Serial.print("SENSORVALUE<RX15>: ");
    Serial.println(data,BIN);
  */

// TOUCHSTATE
  data = readRegister(MTCH6102_TOUCHSTATE);
  //Serial.print("TOUCHSTATE: ");
  //Serial.println(data,BIN);

  //Read the bit for GES (bit 1)
  uint8_t regMode = data;
  gesture_present = bitRead(regMode, 1);
  //Serial.print("GES: ");
  //Serial.println(gesture_present,BIN);

  if (gesture_present == 1) {
    gesture_id = readRegister(MTCH6102_GESTURESTATE);
    Serial.println(gesture_id, BIN);
  }
  
  /*
  data = readRegister(0x11);
  Serial.print("TOUCHX: ");
  Serial.println(data,BIN);
  data = readRegister(0x12);
  Serial.print("TOUCHY: ");
  Serial.println(data,BIN);
// TOUCHLSB
  data = readRegister(0x13);
  Serial.print("TOUCHLSB: ");
  Serial.println(data,BIN);
  */
/*
  Serial.print("SENSORVALUE_RX <i>: ");
  for (byte i = 0x80; i < 0x8E; i++) {
     data = readRegister(i);
     
     //Serial.print(i, HEX);
    //Serial.print(" = ");
    Serial.print(data,BIN);
    Serial.print(", ");
  }
  Serial.println(); */
  /*
    data = readRegister(0x80);
    Serial.print("SENSORVALUE<RX0>: ");
    Serial.println(data,BIN);
    data = readRegister(0x8D);
    Serial.print("SENSORVALUE<RX13>: ");
    Serial.println(data,BIN);
    data = readRegister(0x86);
    Serial.print("SENSORVALUE<RX6>: ");
    Serial.println(data,BIN);
*/
    delay(100);
}
